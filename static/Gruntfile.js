
module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        build: {
            root: 'build',
            version: '<%= build.root %>/<%= pkg.version %>',
            bundles: {
                styles: '<%= build.version %>/css/bundle.css',
                sources: '<%= build.version %>/js/bundle.js'
            }
        },
        sources: {
            root: 'src'
        },
        clean: {
            build: { src: [ '<%= build.root %>/*' ] }
        },
        watch: {
            options: {
                interrupt: true
            },
            sources: {
                files: ['<%= sources.root %>/**'],
                tasks: ['development']
            }
        },
        copy: {
            static: {
                expand: true, cwd: '<%= sources.root %>',
                src: [ 'views/**' , 'assets/**'],
                dest: '<%= build.version %>'
            },
            index: {
                options: {
                    process: function(content){
                        return grunt.template.process(content);
                    }
                },
                expand: true, cwd: '<%= sources.root %>',
                src: 'index.html',
                dest: '<%= build.root %>'
            }
        },
        concat: {
            options: {
                sourceMap: true
            },
            sources: {
                src: [  '<%= sources.root %>/js/core/jquery/*.js',
                        '<%= sources.root %>/js/core/*.js',
                        '<%= sources.root %>/js/modules/**/*.js',
                        '<%= sources.root %>/js/application.js' ,
                        '<%= sources.root %>/js/main/**/*.js' ],
                dest: '<%= build.bundles.sources %>'
            },
            styles: {
                src: [ '<%= sources.root %>/css/*.css' ],
                dest: '<%= build.bundles.styles %>'
            }
        },
        uglify: {
            bundle: {
                files: {
                    '<%= build.bundles.sources %>' : [ '<%= concat.sources.dest %>' ]
                }
            }
        },
        cssmin: {
            bundle: {
                files: {
                    '<%= build.bundles.styles %>' : [ '<%= concat.styles.dest %>' ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['clean', 'copy', 'concat']);
    grunt.registerTask('development', ['default']);
    grunt.registerTask('production', ['default', 'uglify', 'cssmin']);
};