"use strict";

(function () {
    angular.module('mainApplication')
        .service('pathToSvc', [ function PathToService() {
            /**
             * Resuelve el path relativo a la version actual de la aplicacion.
             * @param path
             * @returns {*}
             */
            this.pathTo = function (path) {
                if(!path) {
                    return '';
                }

                return '/' + angular.module('mainApplication').info().version + '/' + path.replace(/^\//, '');
            }
        }]);
})();