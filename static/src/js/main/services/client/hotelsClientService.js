"use strict";

(function () {
    angular.module('mainApplication')
        .service('hotelsClientSvc', [ '$http' , '$timeout' , '$q' , function HotelsClientSvc($http, $timeout, $q) {

            this.getAvailableFilter = function () {
                var defer = $q.defer();

                $timeout(function () {
                    defer.resolve({
                        filter: {
                            order: {value: 'important'},
                            priceRange: [780, 2150],
                            stars: [{count: 25, star: 5, selected: true},
                                {count: 128, star: 4, selected: true},
                                {count: 93, star: 3, selected: true},
                                {count: 31, star: 2, selected: true},
                                {count: 7, star: 1, selected: true}]
                        }
                    });
                }, 200);

                return defer.promise;
            };

            this.getHotels = function(filter){
                return $http.post('/hotels', filter).then(function (response) {
                    return response.data;
                });
            };

        }]);
})();