"use strict";

(function () {
    angular.module('mainApplication')
        .controller('hotelsController', [ '$scope' , 'hotelsClientSvc', function ($scope, hotelsClientSvc) {

            var that = this;
            that.filter = {order: {value:'important'}};

            that.hotels = [];

            that.orders = [{label: 'Más relevantes', value: 'important'},
                           {label: 'Por nombre', value: 'name'},
                           {label: 'Por precio', value: 'price'}];

            function getHotels(filter) {
                hotelsClientSvc.getHotels(filter).then(function (response) {
                    that.hotels = response.hotels;
                });
            }

            function getFilters() {
                hotelsClientSvc.getAvailableFilter().then(function (response) {
                    that.filter= response.filter;
                    getHotels(that.filter);
                });
            }

            (function () {
                getFilters();

                $scope.$watchCollection(function () {
                    return that.filter;
                }, function (nv) {
                    getHotels(nv);
                });

                $scope.$watchCollection(function () {
                    return JSON.stringify(that.filter.stars);
                }, function (nv) {
                    getHotels(that.filter);
                })
            })();
        }]);
})();