"use strict";

(function () {
    angular.module('mainApplication')
        .directive('amGroupChoose', ['pathToSvc',
            /**
             * Definicion de directiva a usarse para seleccionar elementos en distintos grupos.
             * Cada elemento representa un grupo, con una cantidad para el mismo (count).
             * Por defecto, dibujara cada grupo en un span seguido de la propiedad count del mismo entre parentesis.
             * Si se define un template-url, se usara para dibujar el texto de cada grupo, incluido
             * en el contexto de ng-repeat, y cada item estara representado en una variable llamada $item.
             *
             * Ver groupChoose.html para mas informacion.
             * @param pathToSvc
             * @returns {{*}}
             */
            function GroupChooseFactory(pathToSvc){
                return {
                    restrict: 'E',
                    scope: {
                        templateUrl: '@templateUrl',
                        model: '=model',
                        allLabel: '@allLabel'
                    },
                    templateUrl: pathToSvc.pathTo('views/templates/groupChoose.html'),
                    controller: [ '$scope' , function GroupChooose($scope) {

                        $scope.model = $scope.model || [];
                        $scope.all = false;

                        /**
                         * Devuelve la sumatoria de todos los items.count.
                         * @returns {number}
                         */
                        $scope.getAllCount = function () {
                            var allCount = 0;
                            angular.forEach($scope.model, function (item) {
                                allCount += item.count;
                            });
                            return allCount
                        };

                        /**
                         * Devuelve true si todos los items estan seleccionados
                         * @returns {boolean}
                         */
                        $scope.allChecked = function () {
                            return $scope.model.every(function (item) {
                                return item.selected;
                            });
                        };

                        /**
                         * Setea el valor correspondiente al boton de todos los checks.
                         */
                        $scope.checkAll = function () {
                            angular.forEach($scope.model, function (item, index) {
                                item.selected = $scope.all;
                            });
                        };

                        /**
                         * Actualiza el boton de todos los checks.
                         */
                        $scope.updateChecks = function () {
                            $scope.all = $scope.allChecked();
                        };
                    }]
                };
        }]);
})();