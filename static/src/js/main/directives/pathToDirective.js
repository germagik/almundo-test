"use strict";

(function () {
    angular.module('mainApplication')
        .directive('pathTo', function PathToDirective() {
            return {
                restrict: 'A',
                controller: ['$scope' , 'pathToSvc',
                    function PathToDirective($scope, pathToSvc) {

                        /**
                         * Simplemente agrega al scope actual la funcionalidad para
                         * resolver los paths.
                         */
                        $scope.pathTo = pathToSvc.pathTo;
                    }]
            }
        });
})();