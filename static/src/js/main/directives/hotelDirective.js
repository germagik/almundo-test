"use strict";

(function () {
    angular.module('mainApplication')
        .directive('amHotel', [ 'pathToSvc' , function HotelDirectiveFactory(pathToSvc) {
            return {
                restrict: 'E',
                scope: {
                    hotel: '<hotel'
                },
                templateUrl: pathToSvc.pathTo('views/templates/hotel.html')
            };
        }]);
})();
