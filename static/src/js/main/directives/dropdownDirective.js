"use strict";

(function () {
    angular.module('mainApplication')
        .directive('amDropdown', [ 'pathToSvc' , function(pathToSvc) {
            return {
                restrict: 'E',
                scope: {
                    title: '@title',
                    icon: '@icon',
                    open: '<open'
                },
                transclude: true,
                templateUrl: pathToSvc.pathTo('views/templates/dropdown.html')
            };
        }]);
})();