"use strict";

(function(){

    /**
     * Busca en el head del documento, el tag meta cuyo atributo name sea 'version'
     * y devuelve el contenido (atributo content del mismo).
     * @returns la version de la aplicación.
     */
    function getVersion(){

        var metas = window.document.head.getElementsByTagName('meta');
        for(var i = 0; i < metas.length; i++) {
            var meta = metas[i];
            if(meta.getAttribute('name') == 'version') {
                return meta.getAttribute('content');
            }
        }

        return '';
    }

    /**
     * Define la info con su version para el module mainApplication.
     */
    angular.module('mainApplication')
        .info({
            version: getVersion()
        })
})();
