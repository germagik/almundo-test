"use strict";

(function () {
    angular.module('modResponsive', [])
        .provider('modResponsiveSvc', function ResponsiveProvider() {
            var minMedium = 601,
                minLarge = 993;

            /**
             * Configura el breakpoint a ser usado como minimo (inclusive) para el
             * tamaño large del servicio de responsive.
             * @param minLargeValue
             */
            this.setMinimumLargeBreakpoint = function (minLargeValue) {
                minLarge = minLargeValue;
            };

            /**
             * Configura el breakpoint a ser unsado como minimo (inclusive) para el
             * tamaño medium del servicio de responsive.
             * @param minMediumValue
             */
            this.setMinimumMediumBreakpoint = function (minMediumValue) {
                minMedium = minMediumValue;
            };

            /**
             * Funcion factory que devuelve el servicio con las banderas
             * necesarias para los 3 breakpoints de responsive, por defecto
             * usa los valores de w3.
             * @type {[*]}
             */
            this.$get = ['$window' , '$rootScope', function ($window, $rootScope) {
                return new ResponsiveService($window, $rootScope);
            }];

            /**
             * Clase encargada de informar el estado actual de los breackpoints.
             * @param $window
             * @param $rootScope
             * @constructor
             */
            function ResponsiveService($window, $rootScope) {
                var windowElement = angular.element($window);

                /**
                 * Devuelve true si la pantalla esta por debajo del breakpoint
                 * small configurado.
                 * @returns {boolean}
                 */
                this.isSmall = function () {
                    return getWidth() < minMedium;
                };

                /**
                 * Devuelve true si la pantalla esta entre el breakpoint small
                 * y el breakpoint medium configurado.
                 * @returns {boolean}
                 */
                this.isMedium = function () {
                    var width = getWidth();
                    return width < minLarge && width >= minMedium;
                };

                /**
                 * Devuelve true si la pantalla esta sobre el breakpoint large
                 * configurado.
                 * @returns {boolean}
                 */
                this.isLarge = function () {
                    return getWidth() >= minLarge;
                };

                /**
                 * Devuelve el ancho actual de la pantalla.
                 * @returns {Number}
                 */
                function getWidth() {
                    return windowElement[0].innerWidth;
                }

                /**
                 * Inicializacion.
                 */
                (function () {
                    windowElement.bind('resize', function () {
                        $rootScope.$apply();
                    });
                }).call(this);
            }
        })
        .directive('modResponsive', function ResponsiveDirective() {
            return {
                restrict: 'A',
                /**
                 * Controlador que simplemente agrega el servicio de responsive al scope en donde se utilize.
                 */
                controller: [ '$scope' , 'modResponsiveSvc' ,
                    function ResponsiveController($scope, modResponsiveSvc) {
                        $scope.responsive = modResponsiveSvc;
                    }]
            }
        })
})();