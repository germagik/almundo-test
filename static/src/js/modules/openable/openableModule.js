"use strict";

(function () {
    angular.module('modOpenable', [])
        .directive('modOpenable', function () {
            return {
                restrict: 'A',
                scope: true,
                controller: [ '$scope' , '$attrs' , function OpenableController($scope, $attrs) {
                    $scope[$attrs.modOpenable] = new OpenableHolder();
                    
                    function OpenableHolder() {
                        var open = false;

                        this.isOpen = function (isOpen) {
                            if(arguments.length > 0){
                                open = isOpen;
                            }
                            return open;
                        };

                        this.switch = function () {
                            return this.isOpen(!open);
                        };

                        this.close = function () {
                            return this.isOpen(false);
                        };

                        this.open = function () {
                            return this.isOpen(true);
                        };
                    }
                }]
            }
        });
})();