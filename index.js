var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var opn = require('opn');

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static('static/build'));

require('./server/server.js')(app);

require("http").createServer(app).listen(8080);

console.log("Servicio ejecutandose en el puerto 8080...");

opn('http://localhost:8080');