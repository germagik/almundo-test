
module.exports = function (app) {
    app.post('/hotels', function (req, res) {
        var filter = req.body, response = {hotels: require('./mock.json').hotels};

        if(filter.name != null) {
            response.hotels = response.hotels.filter((hotel)=>{
                var reg = new RegExp("^.*" + filter.name + ".*$", "i");
                return reg.test(hotel.name);
            });
        }

        if(filter.priceRange != null) {
            response.hotels = response.hotels.filter((hotel)=>{
                return hotel.price >= filter.priceRange[0] && hotel.price <= filter.priceRange[1];
            });
        }

        if(filter.stars && filter.stars.length) {
            response.hotels = response.hotels.filter((hotel) => {
                return filter.stars.find((star)=>star.star == hotel.stars).selected;
            });
        }

        if(filter.order && filter.order.value != 'important'){
            if(filter.order.value == 'name'){
                response.hotels = response.hotels.sort((before, after)=>{
                    return before.name > after.name;
                });
            }

            if(filter.order.value == 'price'){
                response.hotels = response.hotels.sort((before, after)=>{
                    return before.price > after.price;
                });
            }
        }
        res.send(response);
    });
};